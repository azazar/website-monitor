package com.manimarank.websitemonitor.data.db

import android.content.Context
import androidx.room.*
import androidx.room.migration.AutoMigrationSpec

@Database(
    entities = [WebSiteEntry::class],
    version = 3,
    exportSchema = true,
    autoMigrations = [
        AutoMigration (from = 1, to = 2),
        AutoMigration (from = 2, to = 3, spec = DbHelper.MigrationSpec::class)
    ]
)
abstract class DbHelper: RoomDatabase() {

    abstract fun webSiteEntryDao(): WebSiteEntryDao

    companion object{
        private var INSTANCE: DbHelper? = null

        fun getInstance(context: Context): DbHelper? {
            if (INSTANCE == null) {
               synchronized(DbHelper::class) {
                   INSTANCE = Room.databaseBuilder(context,
                       DbHelper::class.java,
                       "web_site_monitor_db")
                       .build()
               }
            }
            return INSTANCE
        }
    }

    @DeleteColumn(
        tableName = "web_site_entry",
        columnName = "status"
    )
    class MigrationSpec : AutoMigrationSpec {}

}